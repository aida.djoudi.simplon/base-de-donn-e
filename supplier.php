
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>projet bdd</title>
        <link rel="stylesheet" href="global.css">
        <link 
        rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous"
        />
        </head>
    <body>
    <?php include "header.php";?>
    <br/>
    <br/>
            <main>
                <section >
                    <?php   
                    try
                    {
                    //afficher les erreure//
                    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                    $bdd = new PDO('mysql:host=localhost;dbname=projectbdd', 'root', 'root',$pdo_options);
                   // echo "Connection à la base de données reussie <br>";
                    }
                    catch(PDOException $e)
                    {
                    die('Erreur :'.$e->getMessage());
                    }     
                    

 // recuperer les resultats apartir de la table crée et on peut metre soit pdostatement soit n'importe quel nom//
    $pdoStatement=$bdd->query('select * from product');
 // afficher les resultats //
    $result = $pdoStatement->fetchAll();
 // organiser l'afichage du resultat'//
                    ?>
   
  <div class="container">
    <table class="table">
    <thead>
    <tr>
    <th scope="col  ">Nom de fournisseurs</th>
    <th scope="col ">site</th>
    </tr>
    <?php foreach ($result as $value): ?>
    </thead>
    <tbody>
    <tr>
    <td><?=$value["Name_supplier"]?></td>
    <td> <?=$value["product_site"]?></td>
    </tr>
    </tbody>
    <?php endforeach; ?>
    </table>
    </div>
    </article>
    </section>
    </main>  
   
    <?php include "footer.php"; ?>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
    </html>
