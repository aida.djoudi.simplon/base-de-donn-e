<?php
require("vendor/autoload.php");
//use \PDO;
//connecter une base de donnée avec php //
try
{
//afficher les erreure//
$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
$bdd = new PDO('mysql:host=localhost;dbname=projectbdd', 'root', 'root',$pdo_options);
echo "Connection à la base de données reussie <br>";
}
catch(PDOException $e)
{
die('Erreur :'.$e->getMessage());
}                           
// read excel spreadsheet
$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$my_file = "./database6.xlsx";

/////////////////////////////////////////table customer(1)//////////////////////////////////////////
if($reader) {
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($my_file);  
$sheetData = $spreadsheet->getActiveSheet(1)->toArray();

// print_r($sheetData);

foreach($sheetData as $row) {
//get columns
$id_customer = isset($row[0]) ? $row[0] : "";
$first_name = isset($row[1]) ? $row[1] : "";
$last_name = isset($row[2]) ? $row[2] : "";
$email = isset($row[3]) ? $row[3] : "";
$phone = isset($row[4]) ? $row[4] : "";
$city = isset($row[5]) ? $row[5] : "";
$adress = isset($row[6]) ? $row[6] : "";
$country = isset($row[7]) ? $row[7] : "";

// insert customer
$query = "INSERT INTO customer(id_customer, first_name, last_name, email, phone, city, adress, country) values(?,?,?,?,?,?,?,?)";
$prep = $bdd->prepare($query);
$prep->execute(array($id_customer,$first_name ,$last_name,$email,$phone,$city,$adress,$country));
}}
////////////////////////////////////product table(2)///////////////////////////////////////////////////////

if($reader) {
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($my_file);  
    $sheetData = $spreadsheet->getSheetByName('Produits')->toArray();
    print_r($sheetData);
    
    foreach($sheetData as $row) {

    // get columns
    $id_product = isset($row[0]) ? $row[0] : "";
    $name_product = isset($row[1]) ? $row[1] : "";
    $Name_supplier = isset($row[2]) ? $row[2] : "";
    $product_description = isset($row[3]) ? $row[3] : "";
    $product_site = isset($row[4]) ? $row[4] : "";
    $price = isset($row[6]) ? $row[6] : "";
    // insert product
    $query = "INSERT INTO product(id_product, name_product,Name_supplier,price, product_description, product_site) values(?,?,?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_product, $name_product,$Name_supplier,$price, $product_description, $product_site));
    }
    } 

///////////////////////////////////////table tag(3)//////////////////////////////////////////
if($reader) {
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($my_file);  
$sheetData = $spreadsheet->getSheetByName('Produits')->toArray();

print_r($sheetData);

foreach($sheetData as $row) {
//  get columns
$id_tag = isset($row[0]) ? $row[0] : "";
$tag = isset($row[5]) ? $row[5] : "";

// insert tag
$query = "INSERT INTO tag(id_tag, tag) values(?,?)";
$prep = $bdd->prepare($query);
$prep->execute(array($id_tag, $tag));
}}

// /////////////////////////////////////////table ordered(4)//////////////////////////////////////////////
//var_dump($bdd);
if($reader) {
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($my_file);  
    $sheetData = $spreadsheet->getSheetByName('Ventes')->toArray();
    // print_r($sheetData);
    foreach($sheetData as $row) {
    //get columns
    $id_ordered = isset($row[0]) ? $row[0] : "";
    $id_customer =isset($row[1]) ? $row[1] : "";
    $date_time = isset($row[3]) ? $row[3] : "";
    // print_r($id_ordered);
    // print_r($id_customer);
    // print_r($date_time);
    // // insert ordered
    $query = "INSERT INTO ordered (id_customer, date_time) VALUES (:id_customer, :date_time)";
    $prep = $bdd->prepare($query);
    // $id_c = intval(trim($id_customer,"customer-"));
    $prep->bindValue(":id_customer",$id_c);
    $prep->bindValue(":date_time",$date_time);
    $prep->execute();
    $prep->closeCursor();
}
}
///////////////////////////////////////order_product(5)////////////////////////////////////////////////////////////////

if($reader) {
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($my_file);  
        $sheetData = $spreadsheet->getSheetByName('Ventes')->toArray();
        // print_r($sheetData);
        foreach($sheetData as $row) {
        //get columns
        $id_ordered_product = isset($row[0]) ? $row[0] : "";
        $id_ordered = isset($row[0]) ? $row[0] : "";
        $id_product =isset($row[2]) ? $row[2] : "";
        $color = isset($row[4]) ? $row[4] : "";
        $price=null;
        // print_r($id_ordered);
        // print_r($id_customer);
        // print_r($date_time);
        // // insert ordered
        $query = "INSERT INTO ordered_product (id_ordered,id_product,color,price) VALUES (:id_ordered,:id_product,:color,:price)";
        $prep = $bdd->prepare($query);
        $id_p = intval(trim($id_product,"product-"));
        $prep->bindValue(":id_ordered",$id_ordered);
        $prep->bindValue(":id_product",$id_p);
        $prep->bindValue(":color",$color);
        $prep->bindValue(":price",$price);
        $prep->execute();
        $prep->closeCursor();
    }
    }
/////////////////////////////////////product_tag(6)////////////////////////////////////////////////////////////////

if($reader) {
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($my_file);  
        $sheetData = $spreadsheet->getSheetByName('Produits')->toArray();
        print_r($sheetData);
        foreach($sheetData as $row) {
        //get columns
        $id_product_tag=isset($row[0]) ? $row[0] : "";
        $id_tag = isset($row[0]) ? $row[0] : "";
        $id_product =isset($row[0]) ? $row[0] : "";
        $id_tag = isset($row[0]) ? $row[0] : "";
        
        print_r($id_ordered);
        print_r($id_customer);
        print_r($date_time);
        // insert ordered
        $t=" SELECT id_product FROM product WHERE id_product = $id_product";
        print_r($t);
        $query = "INSERT INTO product_tag (id_product,id_tag) VALUES (:id_product,:id_tag)";
        $prep = $bdd->prepare($query);
        $prep->bindValue(":id_tag",$id_tag);
        $prep->bindValue(":id_product",$id_product);
        $prep->execute();
        $prep->closeCursor();
    }
    }

